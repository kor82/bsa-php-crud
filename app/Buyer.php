<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{ 
	protected $fillable = [
		'name', 'surname', 'country', 'city', 'address_line', 'phone'
	];

	/**
	 * Buyer can have many orders
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orders()
	{
		return $this->hasMany('\App\Order');
	}

}
