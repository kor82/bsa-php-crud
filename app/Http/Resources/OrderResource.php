<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $order = [
            'orderId' => $this->id,
            'orderDate' => $this->created_at->format('d.m.Y'),
            'orderSum' => $this->calculateOrderSumInDollars(),
            'orderItems' => [],
            'buyer' => [
                'buyerFullName' =>  $this->getBuyerFullName(),
                'buyerAddress' =>  $this->getBuyerAddress(),
                'buyerPhone' =>  $this->buyer->phone
            ],         
        ];

        $this->orderItems->map(function($orderItem) use (&$order) {
            $order['orderItems'][] = [
                'productName' => $orderItem->product->name,
                'productQty' => $orderItem->quantity,
                'productPrice' => $orderItem->price / 100,
                'productDiscount' => $orderItem->discount.'%',
                'productSum' =>  round($orderItem->calculateSumInCents(
                                                $orderItem->price,
                                                $orderItem->quantity,
                                                $orderItem->discount
                                      ) / 100, 2)
            ];
        });

        return $order;
    }

    /**
     * Calculate Order Sum In Dollars
     * 
     * @return double
     */
    protected function calculateOrderSumInDollars()
    {
        $sum = $this->orderItems->map(function($orderItem) {
            return $orderItem->calculateSumInCents(
                $orderItem->price,
                $orderItem->quantity,
                $orderItem->discount
            );
        })->sum() / 100;

        return round($sum, 2);
    }

    /**
     * Get Buyer Full Name
     * 
     * @return string
     */
    protected function getBuyerFullName()
    {
        return $this->buyer->name.' '.$this->buyer->surname;
    }

    /**
     * Get Buyer Address
     * 
     * @return string
     */
    protected function getBuyerAddress()
    {
        $buyer = $this->buyer;
        return "$buyer->country, $buyer->city, $buyer->address_line";
    }

}
