<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\OrderItem;
use App\Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with(['orderItems.product', 'buyer'])->get();

        return $orders->map(function($order) {
            return new OrderResource($order);
        });
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'buyerId' => ['required', 'integer', 'exists:buyers,id'],
            'orderItems.*.productId' => ['required', 'integer', 'exists:my_products,id'],
            'orderItems.*.productQty' => ['required', 'integer', 'min:1'],
            'orderItems.*.productDiscount' => ['required', 'integer'],
        ]);

        $order = Order::create(['buyer_id' => $request->buyerId]);

        foreach ($request->orderItems as $item) {
            $orderItem = OrderItem::findOrFail($item['productId']);
            $order->orderItems()->save($orderItem);
        }

        return response()->json($order);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return new OrderResource($order->load(['orderItems.product', 'buyer']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'orderId' => ['required', 'integer', 'exists:orders,id'],
            'orderItems.*.productId' => ['required', 'integer', 'exists:my_products,id'],
            'orderItems.*.productQty' => ['required', 'integer', 'min:1'],
            'orderItems.*.productDiscount' => ['required', 'integer'],
        ]);

        $order->load(['orderItems.product', 'buyer']);

        $itemIds = $order->orderItems->map(function($item){
            return $item->product->id;
        })->toArray();

        foreach ($request->orderItems as $item) {
            
            if (in_array($item['productId'], $itemIds)) {
                $order->orderItems
                      ->firstWhere('product_id', $item['productId'])
                      ->update([
                        'quantity' => $item['productQty'],
                        'discount' => $item['productDiscount'],
                        ]);                
            } else {
                $order->orderItems()->create([
                    'product_id' => $item['productId'],
                    'quantity' => $item['productQty'],
                    'discount' => $item['productDiscount'],
                    'price' => Product::findOrFail($item['productId'])->price
                ]);                
            }

        }

        return new OrderResource($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        return ['result' => $order->delete() ? 'success' : 'fail'];
    }
}
