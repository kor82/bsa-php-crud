<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['buyer_id', 'seller_id'];

	/**
	 * Order belongs to Buyer
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function buyer()
	{
		return $this->belongsTo('\App\Buyer')->withDefault();
	}

	/**
	 * Order contain many items
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orderItems()
	{
		return $this->hasMany('\App\OrderItem');
	}	

	/**
	 * Order belongs to Seller
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function seller()
	{
		return $this->belongsTo('\App\Seller')->withDefault();
	}

}
