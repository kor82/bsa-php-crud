<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
    	'order_id', 'product_id', 'quantity', 'price', 'discount'
    ];

    /**
     * Calculate Sum In Cents
     * 
     * @param  integer  $priceInCents
     * @param  integer  $quantity
     * @param  integer $discount
     * @return float sum
     */
    public function calculateSumInCents($priceInCents, $quantity, $discount = 0)
    {
    	$sum = $priceInCents * (100 - $discount)/100 * $quantity;

        return round($sum, 2);
    }

    /**
     *  OrderItem belongs to Order
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
		return $this->belongsTo('\App\Order')->withDefault();	
    }

    /**
     *  OrderItem belongs to Product
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('\App\Product')->withDefault();  
    }

}
