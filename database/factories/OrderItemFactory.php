<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {

	$randomProduct = App\Product::whereAvailable(1)
						->orderByRaw('RAND()')->first();

    return [
		'product_id' => $randomProduct->id,
		'quantity' => $faker->numberBetween(1, 100),
		'price' => $randomProduct->price,
		'discount' => $faker->randomElement([5,10,15])
    ];
});
