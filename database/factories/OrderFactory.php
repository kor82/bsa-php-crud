<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;
use App\Seller;

$factory->define(Order::class, function (Faker $faker) {

	$randomSellerId = Seller::orderByRaw('RAND()')->first()->id;

    return [
		'buyer_id' => function () {
			return factory(\App\Buyer::class)->create()->id;
		},
		'seller_id' => $randomSellerId
    ];
});
