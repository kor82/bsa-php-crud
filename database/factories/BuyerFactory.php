<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
		'name' => explode(' ', $faker->name)[1], 
		'surname' => explode(' ', $faker->name)[0],
		'country' => $faker->country,
		'city' => $faker->city,
		'address_line' => $faker->address,
		'phone' => $faker->phoneNumber,
    ];
});
