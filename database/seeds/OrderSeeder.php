<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Order::class, 10)->create()
            ->each(function ($order){
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, rand(2,5))->make()
                );
            });
    }
}
